FROM php:8.2-apache-bookworm

# Install Symfony CLI
RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash
RUN apt-get update && apt-get install -y symfony-cli
RUN symfony check:requirements

# Enable mod_rewrite
RUN a2enmod rewrite && service apache2 restart

# Install Composer
RUN apt-get update && apt-get install -y git unzip p7zip-full
COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /var/www/project

# Install extra Composer dependencies
RUN apt-get update && apt-get install -y libzip-dev
RUN docker-php-ext-install zip pdo_mysql opcache

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_NO_INTERACTION=1

# Copy app and install dependencies
COPY . .

RUN composer install

# Create cache directories and set their permissions
RUN mkdir -p var/{cache/dev,cache/prod,log}
RUN chmod 777 -R var/

# Copy Apache HTTP Server config
COPY .docker/etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf