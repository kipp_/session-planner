# Session planner

# Contents

- [Setup](#setup)
  - [Development](#development)
  - [Production](#production)
    - [First setup](#first-setup)
    - [Updating](#updating)
    - [Common steps](#common-steps)
- [Troubleshooting](#troubleshooting)
  - [HTTP status 500](#http-status-500)
  - [HTTP status 404 on any route](#http-status-404-on-any-route)

---

# Setup

## Development

1. Run `docker compose up -d` to start containers
2. Run `docker compose exec -it app symfony console doctrine:migrations:migrate` to setup DB

---

## Production

# First setup

1. Clone the project and `cd` into it's directory
2. Create a `.env.local` file and create a `MARIADB_PASSWORD` env. variable inside it for database password.
3. Do the [common steps](#common-steps) and then come back
4. Setup your user account by running the following command and following instructions:

```bash
docker compose exec -it app \
  php bin/console app:user:create
```

## Updating

1. Pull changes by running `git pull`
2. Rebuild containers by running:

```bash
docker compose build
```

3. Continue with [common steps](#common-steps)

## Common steps

1. If container's aren't running yet, start them:

```bash
docker compose \
  -f docker-compose.yml \
  -f docker-compose.prod.yml \
  up -d
```

2. Run database migrations:

```bash
docker compose exec -it app \
  php bin/console doctrine:migrations:migrate
```

3. Clear the cache:

```bash
docker compose exec -it app \
  php bin/console cache:clear
```

4. Warm up the cache:

```bash
docker compose exec -it app \
  php bin/console cache:warmup
```

---

# Troubleshooting

## HTTP status 500

Usually happens becase the app cannot create cache / log files inside the `var/` directory.

To fix, run `docker compose exec app chmod -R 777 var/`
