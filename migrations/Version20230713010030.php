<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230713010030 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE registration_invitation (id INT AUTO_INCREMENT NOT NULL, issuer_id INT NOT NULL, invitation_code VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_CC26A88BBA14FCCC (invitation_code), INDEX IDX_CC26A88BBB9D6FEE (issuer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE registration_invitation ADD CONSTRAINT FK_CC26A88BBB9D6FEE FOREIGN KEY (issuer_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE game_date ADD CONSTRAINT FK_418B5B5638066306 FOREIGN KEY (user_game_entity_id) REFERENCES user_game (id)');
        $this->addSql('ALTER TABLE game_invitation ADD CONSTRAINT FK_1FC1A64F81C5F0B9 FOREIGN KEY (user_entity_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE game_invitation ADD CONSTRAINT FK_1FC1A64F5383BC5F FOREIGN KEY (game_entity_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE user_game ADD CONSTRAINT FK_59AA7D4581C5F0B9 FOREIGN KEY (user_entity_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE user_game ADD CONSTRAINT FK_59AA7D455383BC5F FOREIGN KEY (game_entity_id) REFERENCES game (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE registration_invitation DROP FOREIGN KEY FK_CC26A88BBB9D6FEE');
        $this->addSql('DROP TABLE registration_invitation');
        $this->addSql('ALTER TABLE game_invitation DROP FOREIGN KEY FK_1FC1A64F81C5F0B9');
        $this->addSql('ALTER TABLE game_invitation DROP FOREIGN KEY FK_1FC1A64F5383BC5F');
        $this->addSql('ALTER TABLE user_game DROP FOREIGN KEY FK_59AA7D4581C5F0B9');
        $this->addSql('ALTER TABLE user_game DROP FOREIGN KEY FK_59AA7D455383BC5F');
        $this->addSql('ALTER TABLE game_date DROP FOREIGN KEY FK_418B5B5638066306');
    }
}
