// Change page color scheme to the one of the user's OS preference.
(() => {
  /**
   * @param {boolean} isDark is the color scheme dark?
   */
  const updateColorScheme = (isDark) =>
    document.documentElement.setAttribute(
      "data-bs-theme",
      isDark ? "dark" : "light"
    );

  const darkColorScheme = window.matchMedia("(prefers-color-scheme: dark)");

  updateColorScheme(darkColorScheme.matches);

  darkColorScheme.addEventListener("change", (e) =>
    updateColorScheme(e.matches)
  );
})();
