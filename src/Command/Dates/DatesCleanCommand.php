<?php

namespace App\Command\Dates;

use App\Repository\GameDateRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:dates:clean',
    description: 'Clean registered dates that are in the past.',
)]
class DatesCleanCommand extends Command
{
    private GameDateRepository $gameDateRepository;

    public function __construct(GameDateRepository $gameDateRepository)
    {
        parent::__construct();
        $this->gameDateRepository = $gameDateRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $affectedRows = $this->gameDateRepository
            ->createQueryBuilder("gd")
            ->where("gd.date < :date")
            ->setParameter("date", new \DateTimeImmutable("today"))
            ->delete()
            ->getQuery()
            ->execute();

        $io->success(
            $affectedRows > 0
                ? "Deleted $affectedRows dates!"
                : "No dates deleted!"
        );

        return Command::SUCCESS;
    }
}
