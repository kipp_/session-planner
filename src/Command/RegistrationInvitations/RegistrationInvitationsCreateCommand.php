<?php

declare(strict_types=1);

namespace App\Command\RegistrationInvitations;

use App\Service\RegistrationInvitationService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:registration-invitations:create',
    description: 'Create a new registration invitation',
)]
class RegistrationInvitationsCreateCommand extends Command
{
    private RegistrationInvitationService $invitationService;

    public function __construct(RegistrationInvitationService $invitationService)
    {
        parent::__construct();
        $this->invitationService = $invitationService;
    }

    protected function configure(): void
    {
        $this->addArgument('issuer', InputArgument::OPTIONAL, 'Invitation issuer username');
    }

    /**
     * Get issuer username from the argument.
     * 
     * If no argument was passed, prompt the user for it.
     *
     * @param SymfonyStyle $io      SymfonyStyle instance
     * @param InputInterface $input InputInterface instance
     * 
     * @throws \Exception thrown if the issuer username is blank
     * 
     * @return string issuer username
     */
    private function getIssuerUsername(SymfonyStyle $io, InputInterface $input): string
    {
        $issuer = $input->getArgument("issuer");

        if (!$issuer) {
            $issuer = $io->ask("Enter invitation issuer's username") ?? "";

            if ($issuer == "") throw new \Exception("Please enter a username.");
        }

        return $issuer;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $issuerUsername = $this->getIssuerUsername(io: $io, input: $input);

            $invitation = $this->invitationService->createInvitation(issuerUsername: $issuerUsername);
            $invitationCode = $invitation->getInvitationCode();

            $io->success("Invitation created! Your code: $invitationCode");
            return Command::SUCCESS;
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
