<?php

declare(strict_types=1);

namespace App\Command\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:user:create',
    description: 'Create a new user',
)]
class UserCreateCommand extends Command
{
    private UserPasswordHasherInterface $userPasswordHasher;
    private EntityManagerInterface $entityManager;

    public function __construct(
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
        $this->userPasswordHasher = $userPasswordHasher;
        $this->entityManager = $entityManager;
    }

    /**
     * Read text input.
     *
     * @param SymfonyStyle $io      SymfonyStyle instance
     * @param string $question      Question to ask the user
     * @param string $errorMessage  Exception error message to throw if the input is blank
     * 
     * @throws \Exception thrown if the user input is blank
     * 
     * @return string user input
     */
    private function readText(SymfonyStyle $io, string $question, string $errorMessage): string
    {
        $text = trim($io->askQuestion(new Question($question)) ?? "");

        if ($text == "") throw new \Exception($errorMessage);

        return $text;
    }

    /**
     * Read password with confirmation.
     *
     * @param SymfonyStyle $io  SymfonyStyle instance
     * 
     * @throws \Exception thrown if the password is empty, contains spaces
     * or if the confirmation password doesn't match.
     * 
     * @return string chosen user password
     */
    private function readPassword(SymfonyStyle $io): string
    {
        $password = $io->askHidden("Enter a password") ?? "";
        $hasSpaces = str_contains($password, " ");

        if ($password == "") throw new \Exception("Please, choose a password.");
        if ($hasSpaces) throw new \Exception("Passwords cannot contain spaces!");

        $confirmation = $io->askHidden("Enter the password again") ?? "";

        if ($confirmation != $password) throw new \Exception("Passwords do not match!");

        return $password;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $username = $this->readText(
                io: $io,
                question: "Enter a username",
                errorMessage: "Please, enter a username."
            );

            $displayName = $this->readText(
                io: $io,
                question: "Enter a display name",
                errorMessage: "Please, enter a display name."
            );

            $password = $this->readPassword(io: $io);

            $user = new User();
            $user->setUsername($username);
            $user->setDisplayName($displayName);
            $user->setPassword(
                $this->userPasswordHasher->hashPassword(
                    user: $user,
                    plainPassword: $password,
                )
            );

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $io->success("User created!");
            return Command::SUCCESS;
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
