<?php

declare(strict_types=1);

namespace App\Command\User;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:user:delete',
    description: 'Delete a user by their username',
)]
class UserDeleteCommand extends Command
{
    private UserRepository $userRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::OPTIONAL, 'Username of the user to delete')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Skip asking for confirmation');
    }

    /**
     * Get a username from the command argument.
     * 
     * If no username was passed, prompt the user to enter it.
     *
     * @param SymfonyStyle $io          SymfonyStyle IO instance
     * @param InputInterface $input     InputInterface instance
     * 
     * @throws \Exception thrown when no username was entered
     * 
     * @return string username of the user to delete
     */
    private function getUsername(SymfonyStyle $io, InputInterface $input): string
    {
        $username = $input->getArgument("username");

        if (!$username) {
            $username = trim($io->askQuestion(new Question("Enter a username of the user to delete")) ?? "");
        }

        if ($username == "") throw new \Exception("Please enter a username.");

        return $username;
    }

    /**
     * Prompt user for confirmation deletion. Defaults to false.
     * 
     * Automatically returns `true` if the `--force` option was passed.
     *
     * @param SymfonyStyle $io          SymfonyStyle IO instance
     * @param InputInterface $input     InputInterface instance
     * 
     * @return bool deletion confirmation
     */
    private function getConfirmation(SymfonyStyle $io, InputInterface $input): bool
    {
        if ($input->getOption("force") !== false) return true;

        $confirmationQuestion = new ConfirmationQuestion(
            question: "Are you sure you want to delete this user?",
            default: false,
        );

        return $io->askQuestion($confirmationQuestion) !== false;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $username = $this->getUsername(io: $io, input: $input);
            $user = $this->userRepository->findByUsername($username);

            if (!$user) throw new \Exception("User not found.");

            $confirmation = $this->getConfirmation(io: $io, input: $input);

            if (!$confirmation) throw new \Exception("Operation canceled.");

            $this->entityManager->remove($user);
            $this->entityManager->flush();

            $io->success("User deleted!");
            return Command::SUCCESS;
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
