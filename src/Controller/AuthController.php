<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    #[Route('/auth', name: 'auth_index')]
    public function index(): Response
    {
        return $this->redirectToRoute('auth_login');
    }

    #[Route("/auth/login", name: "auth_login")]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render("auth/login.html.twig", [
            "error" => $error,
            "last_username" => $lastUsername,
        ]);
    }

    #[Route("/auth/register", name: "auth_register")]
    public function register(
        Request $request,
        Security $security,
        UserService $userService,
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $user = $userService->registerUser(
                    username: $form->get("username")->getData(),
                    displayName: $form->get("displayName")->getData(),
                    plainPassword: $form->get("plainPassword")->getData(),
                    invitationCode: $form->get("invitationCode")->getData(),
                );

                $security->login($user);

                $this->addFlash('success', 'Registration successful!');
                return $this->redirectToRoute('index');
            } catch (\Exception $ex) {
                $this->addFlash("danger", $ex->getMessage());
            }
        }

        return $this->render('auth/register.html.twig', [
            'registrationForm' => $form,
        ]);
    }

    #[Route('/auth/logout', name: 'auth_logout')]
    public function logout(): Response
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}
