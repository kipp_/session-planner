<?php

namespace App\Controller;

use App\Entity\Game;
use App\Form\AddGameDateFormType;
use App\Form\InvitePlayerFormType;
use App\Repository\GameRepository;
use App\Service\GameDateService;
use App\Service\GameInvitationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameDetailsController extends AbstractController
{
    public function __construct(
        private GameRepository $gameRepository,
        private GameDateService $gameDateService,
        private GameInvitationService $gameInvitationService,
    ) {
    }

    #[Route('/game/details/{id}', name: 'game_details')]
    public function details(Game $game, Request $request): Response
    {
        $addGameDateForm = $this->createForm(type: AddGameDateFormType::class);
        $addGameDateForm->handleRequest($request);

        if ($addGameDateForm->isSubmitted() && $addGameDateForm->isValid()) {
            try {
                $this->gameDateService->addGameDate(
                    gameId: $game->getId(),
                    username: $this->getUser()->getUserIdentifier(),
                    date: $addGameDateForm->get("date")->getData(),
                );

                $this->addFlash("success", "Date added!");
                return $this->redirectToRoute("game_details", ["id" => $game->getId()]);
            } catch (\Exception $ex) {
                $this->addFlash("danger", $ex->getMessage());
            }
        }

        $availableDates = $game->getAvailableDates();
        $userDates = $this->gameDateService->getDates(
            gameId: $game->getId(),
            username: $this->getUser()->getUserIdentifier()
        );

        return $this->render('game_details/details.html.twig', [
            "game" => $game,
            "userDates" => $userDates,
            "availableDates" => $availableDates,
            "addGameDateForm" => $addGameDateForm,
        ]);
    }

    #[Route('/game/details/{id}/admin', name: 'game_details_admin')]
    public function admin(Game $game, Request $request): Response
    {
        $userIsDm = $game->getDm()->getUsername() == $this->getUser()->getUserIdentifier();

        if (!$userIsDm) {
            $this->addFlash("danger", "You're not an admin of this game!");
            return $this->redirect($request->headers->get("referer"));
        }

        $playerInvitationForm = $this->createForm(InvitePlayerFormType::class);
        $playerInvitationForm->handleRequest($request);

        if ($playerInvitationForm->isSubmitted() && $playerInvitationForm->isValid()) {
            try {
                $invitation = $this->gameInvitationService->invitePlayer(
                    gameId: $game->getId(),
                    username: $playerInvitationForm->get("username")->getData(),
                    requestorUsername: $this->getUser()->getUserIdentifier(),
                );

                $displayName = $invitation->getUserEntity()->getDisplayName();
                $this->addFlash("success", "$displayName invited!");

                return $this->redirectToRoute("game_details_admin", ["id" => $game->getId()]);
            } catch (\Exception $ex) {
                $this->addFlash("danger", $ex->getMessage());
            }
        }

        return $this->render('game_details/admin.html.twig', [
            "game" => $game,
            "playerInvitationForm" => $playerInvitationForm,
        ]);
    }

    // TODO: Find better path ...damn.
    #[Route(
        path: '/game/details/{id}/admin/invitations/{invitationId}/cancel',
        name: 'game_invitation_cancel'
    )]
    public function adminInvitationCancel(Game $game, int $invitationId): Response
    {
        try {
            $this->gameInvitationService->cancelInvitation(
                gameInvitationId: $invitationId,
                requestorUsername: $this->getUser()->getUserIdentifier(),
            );

            $this->addFlash("success", "Game invitation canceled!");
        } catch (\Exception $ex) {
            $this->addFlash("danger", $ex->getMessage());
        } finally {
            return $this->redirectToRoute("game_details_admin", ["id" => $game->getId()]);
        }
    }
}
