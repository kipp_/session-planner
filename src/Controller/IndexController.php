<?php

namespace App\Controller;

use App\Form\CreateGameFormType;
use App\Repository\GameRepository;
use App\Repository\UserRepository;
use App\Service\GameDateService;
use App\Service\GameInvitationService;
use App\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{

    public function __construct(
        private GameInvitationService $gameInvitationService,
        private GameDateService $gameDateService,
        private GameRepository $gameRepository,
        private UserRepository $userRepository,
        private GameService $gameService,
    ) {
    }

    #[Route('/', name: 'index')]
    public function index(UserRepository $userRepository): Response
    {
        $user = $userRepository->findByUsername($this->getUser()->getUserIdentifier());

        $games = $user->getGames();
        $conflictingDates = $user->getConflictingGameDates();

        return $this->render('index.html.twig', [
            "games" => $games,
            "conflictingDates" => $conflictingDates,
        ]);
    }

    #[Route('/create', name: 'create')]
    public function create(Request $request): Response
    {
        $form = $this->createForm(CreateGameFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $game = $this->gameService->createGame(
                    gameName: (string) $form->get("gameName")->getData(),
                    dmUsername: $this->getUser()->getUserIdentifier(),
                );

                $this->addFlash("success", "Game successfuly created!");

                return $this->redirectToRoute("game_details", ["id" => $game->getId()]);
            } catch (\Exception $ex) {
                $this->addFlash("danger", $ex->getMessage());
            }
        }

        return $this->render("create.html.twig", [
            "form" => $form,
        ]);
    }

    #[Route('/game/date/remove/{gameDateId}', name: 'game_remove_date')]
    public function removeDate(int $gameDateId, Request $request): Response
    {
        try {
            $this->gameDateService->removeGameDate(
                gameDateId: $gameDateId,
                requesterUsername: $this->getUser()->getUserIdentifier(),
            );

            $this->addFlash("success", "Game date removed!");
        } catch (\Exception $ex) {
            $this->addFlash("danger", $ex->getMessage());
        } finally {
            return $this->redirect($request->headers->get("referer"));
        }
    }
}
