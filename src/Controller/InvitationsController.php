<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\GameInvitationService;
use App\Service\RegistrationInvitationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvitationsController extends AbstractController
{
    public function __construct(
        private UserRepository $userRepository,
        private GameInvitationService $gameInvitationService,
        private RegistrationInvitationService $registrationInvitationService,
    ) {
    }

    #[Route('/invitations/games', name: 'invitations_games')]
    public function games(): Response
    {
        $invitations = $this->userRepository
            ->findByUsername($this->getUser()->getUserIdentifier())
            ->getGameInvitations();

        return $this->render('invitations/games.html.twig', [
            "invitations" => $invitations,
        ]);
    }

    #[Route('/invitations/registrations', name: 'invitations_registrations')]
    public function registrations(): Response
    {
        $invitations = $this->userRepository
            ->findByUsername($this->getUser()->getUserIdentifier())
            ->getRegistrationInvitations();

        return $this->render('invitations/registrations.html.twig', [
            "invitations" => $invitations,
        ]);
    }

    #[Route('/invitations/registrations/create', name: 'invitations_registrations_create')]
    public function createRegistrationInvite(): Response
    {
        try {
            $this->registrationInvitationService->createInvitation(
                issuerUsername: $this->getUser()->getUserIdentifier(),
            );

            $this->addFlash("success", "Created new registration invitation!");
        } catch (\Exception $ex) {
            $this->addFlash("danger", $ex->getMessage());
        } finally {
            return $this->redirectToRoute("invitations_registrations");
        }
    }

    #[Route('/invitations/registrations/delete/{invitationId}', name: 'invitations_registrations_delete')]
    public function deleteRegistrationInvitation(int $invitationId): Response
    {
        try {
            $this->registrationInvitationService->deleteInvitation(
                invitationId: $invitationId,
                requestorUsername: $this->getUser()->getUserIdentifier(),
            );

            $this->addFlash("success", "Registration invitation deleted!");
        } catch (\Exception $ex) {
            $this->addFlash("danger", $ex->getMessage());
        } finally {
            return $this->redirectToRoute("invitations_registrations");
        }
    }

    #[Route('/invitations/games/{invitationId}/accept', name: 'invitations_games_accept')]
    public function acceptGameInvitation(int $invitationId): Response
    {
        try {
            $userGame = $this->gameInvitationService->acceptInvitation(
                gameInvitationId: $invitationId,
                requestorUsername: $this->getUser()->getUserIdentifier(),
            );

            $game = $userGame->getGameEntity();
            $gameId = $game->getId();
            $gameName = $game->getName();

            $this->addFlash("success", "You joined $gameName!");

            return $this->redirectToRoute('game_details', ["id" => $gameId]);
        } catch (\Exception $ex) {
            $this->addFlash("danger", $ex->getMessage());
            return $this->redirectToRoute("invitations_games");
        }
    }

    #[Route('/invitations/games/{invitationId}/decline', name: 'invitations_games_decline')]
    public function declineGameInvitation(int $invitationId): Response
    {
        try {
            $this->gameInvitationService->declineInvitation(
                gameInvitationId: $invitationId,
                requestorUsername: $this->getUser()->getUserIdentifier(),
            );

            $this->addFlash("success", "Invitation declined!");
        } catch (\Exception $ex) {
            $this->addFlash("danger", $ex->getMessage());
        } finally {
            return $this->redirectToRoute("invitations_games");
        }
    }
}
