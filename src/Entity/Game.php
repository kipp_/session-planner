<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\PrePersist;

#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: GameRepository::class)]
class Game
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    /** @var Collection<int,UserGame> */
    #[ORM\OneToMany(mappedBy: 'gameEntity', targetEntity: UserGame::class, orphanRemoval: true)]
    private Collection $userGames;

    #[ORM\OneToMany(mappedBy: 'gameEntity', targetEntity: GameInvitation::class, orphanRemoval: true)]
    private Collection $gameInvitations;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    public function __construct()
    {
        $this->userGames = new ArrayCollection();
        $this->gameInvitations = new ArrayCollection();
    }

    #[PrePersist]
    public function addCreatedAt(): void
    {
        if (!$this->createdAt) {
            $this->createdAt = new \DateTimeImmutable();
        }
    }

    /** @return Game[] */
    public function getPlayers(): array
    {
        return $this->userGames
            ->map(fn (UserGame $userGame) => $userGame->getUserEntity())
            ->toArray();
    }

    public function hasPlayer(string $username): bool
    {
        return $this->userGames
            ->map(fn (UserGame $userGame) => $userGame->getUserEntity()->getUsername())
            ->contains($username);
    }

    public function getDm(): User
    {
        return $this->userGames
            ->findFirst(
                fn (int $id, UserGame $userGame) =>
                $userGame->getRole() == UserGameRole::DM
            )
            ->getUserEntity();
    }

    /**
     * Get registered dates in range
     *
     * @param ?int $year    Requested year.
     * @param ?int $month   Requested month.
     * @return GameDate[]
     */
    public function getDatesInRange(int $year, int $month): array
    {
        $start = new \DateTime("@" . mktime(hour: 0, year: $year, month: $month, day: 1));
        $end = new \DateTime("last day of " . $start->format("Y-m"));

        /** @var GameDate[] */
        $gameDates = [];

        $this->userGames
            ->map(
                fn (UserGame $userGame) =>
                $userGame
                    ->getGameDates()
                    ->filter(
                        fn (GameDate $gameDate) =>
                        $start <= $gameDate->getDate() && $gameDate->getDate() <= $end
                    )
                    ->toArray()
            )
            ->forAll(fn (array $dates) => array_push(...$dates));

        return $gameDates;
    }

    /**
     * Get dates when all players are available.
     *
     * @return \DateTime[]  array of available dates
     */
    public function getAvailableDates(): array
    {
        /** @var GameDate[] */
        $gameDates = [];

        foreach ($this->userGames as $userGame) {
            foreach ($userGame->getGameDates() as $gameDate) {
                $gameDates[] = $gameDate;
            }
        }

        /** @var array<string,int> */
        $registrationCount = [];

        foreach ($gameDates as $gameDate) {
            $currentDate = $gameDate->getDate()->format("Y-m-d");
            $registrationCount[$currentDate] = ($registrationCount[$currentDate] ?? 0) + 1;
        }

        $playerCount = $this->userGames->count();

        $availableDates = array_keys(
            array_filter(
                $registrationCount,
                fn (int $registrations, string $date) => $registrations == $playerCount,
                ARRAY_FILTER_USE_BOTH,
            )
        );

        return array_map(fn (string $date) => new \DateTime($date), $availableDates);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, UserGame>
     */
    public function getUserGames(): Collection
    {
        return $this->userGames;
    }

    public function addUserGame(UserGame $userGame): static
    {
        if (!$this->userGames->contains($userGame)) {
            $this->userGames->add($userGame);
            $userGame->setGameEntity($this);
        }

        return $this;
    }

    public function removeUserGame(UserGame $userGame): static
    {
        if ($this->userGames->removeElement($userGame)) {
            // set the owning side to null (unless already changed)
            if ($userGame->getGameEntity() === $this) {
                $userGame->setGameEntity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, GameInvitation>
     */
    public function getGameInvitations(): Collection
    {
        return $this->gameInvitations;
    }

    public function addGameInvitation(GameInvitation $gameInvitation): static
    {
        if (!$this->gameInvitations->contains($gameInvitation)) {
            $this->gameInvitations->add($gameInvitation);
            $gameInvitation->setGameEntity($this);
        }

        return $this;
    }

    public function removeGameInvitation(GameInvitation $gameInvitation): static
    {
        if ($this->gameInvitations->removeElement($gameInvitation)) {
            // set the owning side to null (unless already changed)
            if ($gameInvitation->getGameEntity() === $this) {
                $gameInvitation->setGameEntity(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
