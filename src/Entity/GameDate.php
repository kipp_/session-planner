<?php

namespace App\Entity;

use App\Repository\GameDateRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameDateRepository::class)]
class GameDate
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'gameDates')]
    #[ORM\JoinColumn(nullable: false)]
    private ?UserGame $userGameEntity = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserGameEntity(): ?UserGame
    {
        return $this->userGameEntity;
    }

    public function setUserGameEntity(?UserGame $userGameEntity): static
    {
        $this->userGameEntity = $userGameEntity;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static
    {
        $this->date = $date;

        return $this;
    }
}
