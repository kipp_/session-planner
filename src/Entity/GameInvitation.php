<?php

namespace App\Entity;

use App\Repository\GameInvitationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameInvitationRepository::class)]
class GameInvitation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'gameInvitations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $userEntity = null;

    #[ORM\ManyToOne(inversedBy: 'gameInvitations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Game $gameEntity = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserEntity(): ?User
    {
        return $this->userEntity;
    }

    public function setUserEntity(?User $userEntity): static
    {
        $this->userEntity = $userEntity;

        return $this;
    }

    public function getGameEntity(): ?Game
    {
        return $this->gameEntity;
    }

    public function setGameEntity(?Game $gameEntity): static
    {
        $this->gameEntity = $gameEntity;

        return $this;
    }
}
