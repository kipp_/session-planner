<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['username'], message: 'There is already an account with this username')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $username = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 64)]
    private ?string $displayName = null;

    /** @var Collection<int,UserGame> */
    #[ORM\OneToMany(mappedBy: 'userEntity', targetEntity: UserGame::class, orphanRemoval: true)]
    private Collection $userGames;

    #[ORM\OneToMany(mappedBy: 'userEntity', targetEntity: GameInvitation::class, orphanRemoval: true)]
    private Collection $gameInvitations;

    #[ORM\OneToMany(mappedBy: 'issuer', targetEntity: RegistrationInvitation::class, orphanRemoval: true)]
    private Collection $registrationInvitations;

    public function __construct()
    {
        $this->userGames = new ArrayCollection();
        $this->gameInvitations = new ArrayCollection();
        $this->registrationInvitations = new ArrayCollection();
    }

    /**
     * Get game dates which are registered more than once in various games.
     *
     * @return GameDate[]
     */
    public function getConflictingGameDates(): array
    {
        /** @var array<string,GameDate[]> */
        $gameDates = [];

        foreach ($this->userGames as $userGame) {
            foreach ($userGame->getGameDates() as $gameDate) {
                $date = $gameDate->getDate()->format("Y-m-d");
                $gameDates[$date] = [...($gameDates[$date] ?? []), $gameDate];
            }
        }

        return array_values(
            array_filter(
                array: $gameDates,
                callback: fn (array $entities, string $date) => count($entities) > 1,
                mode: ARRAY_FILTER_USE_BOTH,
            )
        );
    }

    /** @return Game[] */
    public function getGames(): array
    {
        return $this->userGames
            ->map(fn (UserGame $userGame) => $userGame->getGameEntity())
            ->toArray();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): static
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * @return Collection<int, UserGame>
     */
    public function getUserGames(): Collection
    {
        return $this->userGames;
    }

    public function addUserGame(UserGame $userGame): static
    {
        if (!$this->userGames->contains($userGame)) {
            $this->userGames->add($userGame);
            $userGame->setUserEntity($this);
        }

        return $this;
    }

    public function removeUserGame(UserGame $userGame): static
    {
        if ($this->userGames->removeElement($userGame)) {
            // set the owning side to null (unless already changed)
            if ($userGame->getUserEntity() === $this) {
                $userGame->setUserEntity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, GameInvitation>
     */
    public function getGameInvitations(): Collection
    {
        return $this->gameInvitations;
    }

    public function addGameInvitation(GameInvitation $gameInvitation): static
    {
        if (!$this->gameInvitations->contains($gameInvitation)) {
            $this->gameInvitations->add($gameInvitation);
            $gameInvitation->setUserEntity($this);
        }

        return $this;
    }

    public function removeGameInvitation(GameInvitation $gameInvitation): static
    {
        if ($this->gameInvitations->removeElement($gameInvitation)) {
            // set the owning side to null (unless already changed)
            if ($gameInvitation->getUserEntity() === $this) {
                $gameInvitation->setUserEntity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RegistrationInvitation>
     */
    public function getRegistrationInvitations(): Collection
    {
        return $this->registrationInvitations;
    }

    public function addRegistrationInvitation(RegistrationInvitation $registrationInvitation): static
    {
        if (!$this->registrationInvitations->contains($registrationInvitation)) {
            $this->registrationInvitations->add($registrationInvitation);
            $registrationInvitation->setIssuer($this);
        }

        return $this;
    }

    public function removeRegistrationInvitation(RegistrationInvitation $registrationInvitation): static
    {
        if ($this->registrationInvitations->removeElement($registrationInvitation)) {
            // set the owning side to null (unless already changed)
            if ($registrationInvitation->getIssuer() === $this) {
                $registrationInvitation->setIssuer(null);
            }
        }

        return $this;
    }
}
