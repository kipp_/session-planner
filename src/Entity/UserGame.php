<?php

namespace App\Entity;

use App\Repository\UserGameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

enum UserGameRole: string
{
    case DM = "dm";
    case PLAYER = "player";
}

#[ORM\Entity(repositoryClass: UserGameRepository::class)]
class UserGame
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'userGames')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $userEntity = null;

    #[ORM\ManyToOne(inversedBy: 'userGames')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Game $gameEntity = null;

    #[ORM\Column(length: 255, enumType: UserGameRole::class)]
    private ?UserGameRole $role = null;

    #[ORM\OneToMany(mappedBy: 'userGameEntity', targetEntity: GameDate::class, orphanRemoval: true)]
    private Collection $gameDates;

    public function __construct()
    {
        $this->gameDates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserEntity(): ?User
    {
        return $this->userEntity;
    }

    public function setUserEntity(?User $userEntity): static
    {
        $this->userEntity = $userEntity;

        return $this;
    }

    public function getGameEntity(): ?Game
    {
        return $this->gameEntity;
    }

    public function setGameEntity(?Game $gameEntity): static
    {
        $this->gameEntity = $gameEntity;

        return $this;
    }

    public function getRole(): ?UserGameRole
    {
        return $this->role;
    }

    public function setRole(UserGameRole $role): static
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return Collection<int, GameDate>
     */
    public function getGameDates(): Collection
    {
        return $this->gameDates;
    }

    public function addGameDate(GameDate $gameDate): static
    {
        if (!$this->gameDates->contains($gameDate)) {
            $this->gameDates->add($gameDate);
            $gameDate->setUserGameEntity($this);
        }

        return $this;
    }

    public function removeGameDate(GameDate $gameDate): static
    {
        if ($this->gameDates->removeElement($gameDate)) {
            // set the owning side to null (unless already changed)
            if ($gameDate->getUserGameEntity() === $this) {
                $gameDate->setUserGameEntity(null);
            }
        }

        return $this;
    }
}
