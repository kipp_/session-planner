<?php

namespace App\Repository;

use App\Entity\RegistrationInvitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RegistrationInvitation>
 *
 * @method RegistrationInvitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistrationInvitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrationInvitation[]    findAll()
 * @method RegistrationInvitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrationInvitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrationInvitation::class);
    }

    public function save(RegistrationInvitation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RegistrationInvitation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByInvitationCode(string $invitationCode): ?RegistrationInvitation
    {
        return $this->findOneBy([
            "invitationCode" => $invitationCode,
        ]);
    }

    //    /**
    //     * @return RegistrationInvitation[] Returns an array of RegistrationInvitation objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?RegistrationInvitation
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
