<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\GameDate;
use App\Entity\UserGame;
use App\Repository\GameDateRepository;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;

final class GameDateService
{
    public function __construct(
        private GameRepository $gameRepository,
        private GameDateRepository $gameDateRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /** @return GameDate[] */
    public function getDates(int $gameId, ?string $username): array
    {
        $game = $this->gameRepository->find($gameId);

        if (!$game) throw new \Exception("Game not found.");

        if ($username) {
            /** @var ?UserGame */
            $userGame = $game->getUserGames()
                ->findFirst(
                    fn (int $id, UserGame $userGame) =>
                    $userGame->getUserEntity()->getUsername() == $username
                );

            if (!$userGame) throw new \Exception("User is not a part of this game!");

            return $userGame->getGameDates()->toArray();
        }

        /** @var GameDate[] */
        $gameDates = [];

        foreach ($game->getUserGames() as $userGame) {
            array_push($gameDates, ...$userGame->getGameDates()->toArray());
        }

        return $gameDates;
    }

    /**
     * Add a new game date.
     *
     * @param int $userGameId UserGame ID to register the date to.
     * @param \DateTime $date Date to register.
     * 
     * @throws \Exception thrown if a UserGame entity is not found, the dat is in the past
     * or if a date is already registered.
     */
    public function addGameDate(int $gameId, string $username, \DateTime $date): void
    {
        $dateIsInPast = $date < new \DateTime("today");

        if ($dateIsInPast) throw new \Exception("Cannot register date in the past!");

        $game = $this->gameRepository->find($gameId);

        if (!$game) throw new \Exception("Game not found.");

        $userGame = $game
            ->getUserGames()
            ->findFirst(
                fn (int $id, UserGame $userGame) =>
                $userGame->getUserEntity()->getUsername() == $username
            );

        if (!$userGame) throw new \Exception("User is not a member of this group!");

        $dateAlreadyRegistered = $userGame
            ->getGameDates()
            ->findFirst(
                fn (int $id, GameDate $gameDate) =>
                $gameDate->getDate()->format("Y-m-d") == $date->format("Y-m-d")
            ) != null;

        if ($dateAlreadyRegistered) throw new \Exception("This date is already registered!");

        $gameDate = new GameDate();
        $gameDate->setUserGameEntity($userGame);
        $gameDate->setDate($date);

        $this->entityManager->persist($gameDate);
        $this->entityManager->flush();
    }

    /**
     * Remove a GameDate by it's ID.
     *
     * @param integer $gameDateId           GameDate ID
     * @param string $requesterUsername     Username of the deletion request creator
     * 
     * @throws \Exception thrown if a GameDate with passed ID doesn't exist
     */
    public function removeGameDate(int $gameDateId, string $requesterUsername): void
    {
        $gameDate = $this->gameDateRepository->find($gameDateId);

        $requestedBySameUser = $gameDate
            ->getUserGameEntity()
            ->getUserEntity()
            ->getUsername() == $requesterUsername;

        if (!$gameDate || !$requestedBySameUser) {
            throw new \Exception("GameDate not found.");
        }

        $this->entityManager->remove($gameDate);
        $this->entityManager->flush();
    }
}
