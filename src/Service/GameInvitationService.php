<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\GameInvitation;
use App\Entity\UserGame;
use App\Entity\UserGameRole;
use App\Repository\GameInvitationRepository;
use App\Repository\GameRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

final class GameInvitationService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private GameRepository $gameRepository,
        private UserRepository $userRepository,
        private GameInvitationRepository $gameInvitationRepository
    ) {
    }

    /**
     * Invite a player to a game
     *
     * @param integer $gameId               game ID
     * @param string $username              invited player's username
     * @param string $requestorUsername     requestor's username
     * 
     * @throws \Exception thrown if the game is not found, the requestor isn't a DM,
     * the invited user doesn't exist, the invited player is already invited
     * or the invited user is already a member
     * 
     * @return GameInvitation               created invitation
     */
    public function invitePlayer(int $gameId, string $username, string $requestorUsername): GameInvitation
    {
        $game = $this->gameRepository->find($gameId);

        if (!$game) {
            throw new \Exception("Game not found.");
        }

        if ($game->getDm()->getUsername() != $requestorUsername) {
            throw new \Exception("You're not the DM of this game!");
        }

        if ($game->hasPlayer($username)) {
            throw new \Exception("This player is already a member!");
        }

        $user = $this->userRepository->findByUsername($username);

        if (!$user) {
            throw new \Exception("Invited user doesn't exist!");
        }

        $invitationExists = $game->getGameInvitations()
            ->findFirst(
                fn (int $id, GameInvitation $gameInvitation) =>
                $gameInvitation->getUserEntity()->getUserIdentifier() == $user->getUserIdentifier()
            ) != null;

        if ($invitationExists) {
            throw new \Exception("This player is already invited!");
        }

        $gameInvitation = new GameInvitation();
        $gameInvitation->setUserEntity($user);
        $gameInvitation->setGameEntity($game);

        $this->entityManager->persist($gameInvitation);
        $this->entityManager->flush();

        return $gameInvitation;
    }

    /**
     * Cancel a game invitation.
     *
     * @param integer $gameInvitationId Game invitation ID
     * @param string $requestorUsername Requestor's username
     * 
     * @throws \Exception thrown if the invitation doesn't exist or the requestor
     * isn't the game's DM.
     */
    public function cancelInvitation(int $gameInvitationId, string $requestorUsername): void
    {
        $gameInvitation = $this->gameInvitationRepository->find($gameInvitationId);

        if (!$gameInvitation) throw new \Exception("Game invitation not found.");

        $requestorIsDM = $gameInvitation
            ->getGameEntity()
            ->getDm()
            ->getUsername() == $requestorUsername;

        if (!$requestorIsDM) throw new \Exception("You cannot cancel this invitation!");

        $this->entityManager->remove($gameInvitation);
        $this->entityManager->flush();
    }

    /**
     * Decline a game invitation.
     *
     * @param integer $gameInvitationId     game invitation ID
     * @param string $requestorUsername     requestor's username
     * 
     * @throws \Exception thrown if the invitation ins't found or
     * the requestor isn't the invitation's recipient.
     */
    public function declineInvitation(int $gameInvitationId, string $requestorUsername): void
    {
        $gameInvitation = $this->gameInvitationRepository->find($gameInvitationId);

        if (!$gameInvitation) throw new \Exception("Game invitation not found.");

        $requestorIsRecipient = $gameInvitation
            ->getUserEntity()
            ->getUsername() == $requestorUsername;

        if (!$requestorIsRecipient) {
            throw new \Exception("You cannot decline nor accept this invitation.");
        }

        $this->entityManager->remove($gameInvitation);
        $this->entityManager->flush();
    }

    /**
     * Accept a game invitation.
     *
     * @param integer $gameInvitationId Game invitation ID
     * @param string $requestorUsername Requestor's username
     * 
     * @throws \Exception thrown if the invitation doesn't exist or the requestor
     * is not the invitation's recipient
     * 
     * @return UserGame new UserGame entity
     */
    public function acceptInvitation(int $gameInvitationId, string $requestorUsername): UserGame
    {
        $gameInvitation = $this->gameInvitationRepository->find($gameInvitationId);

        if (!$gameInvitation) {
            throw new \Exception("Game invitation not found.");
        }

        $requestorIsRecipient = $gameInvitation
            ->getUserEntity()
            ->getUsername() == $requestorUsername;

        if (!$requestorIsRecipient) {
            throw new \Exception("You cannot accept this invitation!");
        }

        $user = $this->userRepository->findByUsername($requestorUsername);

        if (!$user) {
            throw new \Exception("The requestor was not found.");
        }

        $userGame = new UserGame();
        $userGame->setGameEntity($gameInvitation->getGameEntity());
        $userGame->setUserEntity($user);
        $userGame->setRole(UserGameRole::PLAYER);

        $this->entityManager->remove($gameInvitation);
        $this->entityManager->persist($userGame);
        $this->entityManager->flush();

        return $userGame;
    }
}
