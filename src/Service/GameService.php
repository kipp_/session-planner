<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Game;
use App\Entity\UserGame;
use App\Entity\UserGameRole;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

final class GameService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserRepository $userRepository,
    ) {
    }

    /**
     * Create a new game.
     *
     * @param string $gameName      new game's name
     * @param string $dmUsername    DM's username
     * 
     * @throws \Exception           thrown if a user with the DM's username is not found
     * 
     * @return Game                 newly created game entity instance
     */
    public function createGame(string $gameName, string $dmUsername): Game
    {
        $user = $this->userRepository->findByUsername($dmUsername);

        if (!$user) {
            throw new \Exception("User with the username \"$dmUsername\" not found!");
        }

        $game = new Game();
        $game->setName($gameName);

        $userGame = new UserGame();
        $userGame->setUserEntity($user);
        $userGame->setGameEntity($game);
        $userGame->setRole(UserGameRole::DM);

        $this->entityManager->persist($game);
        $this->entityManager->persist($userGame);
        $this->entityManager->flush();

        return $game;
    }
}
