<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\RegistrationInvitation;
use App\Repository\RegistrationInvitationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

final class RegistrationInvitationService
{
    private const INVITATION_CODE_LENGTH = 64;

    public function __construct(
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager,
        private RegistrationInvitationRepository $invitationRepository,
    ) {
    }

    private function generateInvitationCode(): string
    {
        return substr(bin2hex(random_bytes(64)), 0, self::INVITATION_CODE_LENGTH);
    }

    /**
     * Create new registration invitation.
     *
     * @param string $issuerUsername invitation issuer username
     * 
     * @throws \Exception thrown if the issuer isn't found
     * 
     * @return RegistrationInvitation new registration invitation
     */
    public function createInvitation(string $issuerUsername): RegistrationInvitation
    {
        $issuer = $this->userRepository->findByUsername($issuerUsername);

        if (!$issuer) throw new \Exception("Issuer not found.");

        $invitation = new RegistrationInvitation();
        $invitation->setIssuer($issuer);
        $invitation->setInvitationCode($this->generateInvitationCode());

        $this->entityManager->persist($invitation);
        $this->entityManager->flush();

        return $invitation;
    }

    public function deleteInvitation(string $invitationId, string $requestorUsername): void
    {
        $invitation = $this->invitationRepository->find($invitationId);

        if (!$invitation) throw new \Exception("Registration invitation not found.");

        $issuerIsRequestor = $invitation
            ->getIssuer()
            ->getUsername() == $requestorUsername;

        if (!$issuerIsRequestor) throw new \Exception("You cannot delete this invitation!");

        $this->entityManager->remove($invitation);
        $this->entityManager->flush();
    }
}
