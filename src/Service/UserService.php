<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\RegistrationInvitationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserService
{
    public function __construct(
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager,
        private UserPasswordHasherInterface $userPasswordHasher,
        private RegistrationInvitationRepository $invitationRepository,
    ) {
    }

    /**
     * Register a new user.
     *
     * @param string $username      user's username
     * @param string $displayName   user's display name
     * @param string $plainPassword user's plain text password
     * 
     * @throws \Exception thrown if the username is already taken
     * 
     * @return User new user instance
     */
    public function registerUser(
        string $username,
        string $displayName,
        string $plainPassword,
        string $invitationCode,
    ): User {
        if ($this->userRepository->findByUsername($username)) {
            throw new \Exception("This username is already taken!");
        }

        $invitation = $this->invitationRepository->findByInvitationCode($invitationCode);

        if (!$invitation) throw new \Exception("Invalid invitation code!");

        $this->entityManager->remove($invitation);

        $user = new User();
        $user->setUsername($username);
        $user->setDisplayName($displayName);
        $user->setPassword(
            $this->userPasswordHasher->hashPassword(
                user: $user,
                plainPassword: $plainPassword,
            ),
        );

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
